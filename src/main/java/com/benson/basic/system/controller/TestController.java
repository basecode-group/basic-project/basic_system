package com.benson.basic.system.controller;

import com.benson.basic.system.dto.TestDto;
import com.benson.common.common.entity.ResultPoJo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 测试接口
 *
 * @author zhangby
 * @date 20/10/20 5:48 pm
 */
@RestController
@RequestMapping("/test")
@Api(tags = "测试接口")
public class TestController {

     /**
      * 自定义验证
      * @return
      */
     @GetMapping("/vail")
     @ApiOperation(value = "自定义验证", notes = "自定义验证", produces = "application/json")
     public ResultPoJo vailTest(@Valid TestDto testDto) {
         return ResultPoJo.ok("success");
     }
}
