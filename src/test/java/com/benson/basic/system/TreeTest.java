package com.benson.basic.system;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class TreeTest {
    public static void main(String[] args) {
        // 构建二叉树
        TreeNode treeNode1 = new TreeNode(1);
        TreeNode treeNode2 = new TreeNode(2);
        TreeNode treeNode3 = new TreeNode(3);
        TreeNode treeNode4 = new TreeNode(4);
        TreeNode treeNode5 = new TreeNode(5);
        TreeNode treeNode6 = new TreeNode(6);
        treeNode1.left = treeNode2;
        treeNode1.right = treeNode3;
        treeNode2.left = treeNode4;
        treeNode2.right = treeNode5;
        treeNode3.right = treeNode6;

        // 求二叉树中的节点个数 递归/迭代
//        System.out.println(getNodeNumRec(treeNode1));
//        System.out.println(getNodeNum(treeNode1));
        // 求二叉树的深度: getDepthRec 递归/迭代
//        System.out.println(getDepthRec(treeNode1));
//        System.out.println(getDepth(treeNode1));
        // 前序遍历
//        preorderTraversalRec(treeNode1);
//        System.out.println();
//        preorderTraversal(treeNode1);
        // 中序遍历
        inorderTraversalRec(treeNode1);
        System.out.println();
        inorderTraversal(treeNode1);
        // 后序遍历
//        postorderTraversalRec(treeNode1);
    }

    private static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int val) {
            this.val = val;
        }
    }

    // 求二叉树中的节点个数 递归
    private static int getNodeNumRec(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return getNodeNumRec(root.left) + getNodeNumRec(root.right) + 1;
    }

    // 求二叉树中的节点个数 迭代
    private static int getNodeNum(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int count = 1;
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);

        while (!queue.isEmpty()) {
            TreeNode curQueue = queue.remove();
            if (curQueue.left != null) {
                count++;
                queue.add(curQueue.left);
            }
            if (curQueue.right != null) {
                count++;
                queue.add(curQueue.right);
            }
        }
        return count;
    }

    // 求二叉树的深度: getDepthRec（递归）
    private static int getDepthRec(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return Math.max(getDepthRec(root.left), getDepthRec(root.right)) + 1;
    }

    // 求二叉树的深度: getDepthRec（迭代）
    private static int getDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int depth = 0;
        int currentLevel = 1;
        int nextLevel = 0;

        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);

        // 循环迭代
        while (!queue.isEmpty()) {
            TreeNode curQueue = queue.remove();
            currentLevel--;
            if (curQueue.left != null) {
                nextLevel++;
                queue.add(curQueue.left);
            }
            if (curQueue.right != null) {
                nextLevel++;
                queue.add(curQueue.right);
            }
            if (currentLevel == 0) {
                depth++;
                currentLevel = nextLevel;
                nextLevel = 0;
            }
        }
        return depth;
    }

    // 前序
    private static void preorderTraversalRec(TreeNode root) {
        if (root == null) {
            return;
        }
        System.out.print(root.val + " ");
        preorderTraversalRec(root.left);
        preorderTraversalRec(root.right);
    }

    // 前序
    private static void preorderTraversal(TreeNode root) {
        if (root == null) {
            return;
        }

        Stack<TreeNode> stack = new Stack<>();
        stack.add(root);

        while (!stack.isEmpty()) {
            TreeNode curTreeNode = stack.pop();
            System.out.print(curTreeNode.val +" ");
            if (curTreeNode.right != null) {
                stack.add(curTreeNode.right);
            }
            if (curTreeNode.left != null) {
                stack.add(curTreeNode.left);
            }
        }
    }

    // 中序
    private static void inorderTraversalRec(TreeNode root) {
        if (root == null) {
            return;
        }
        inorderTraversalRec(root.left);
        System.out.print(root.val +" ");
        inorderTraversalRec(root.right);
    }

    // 中序
    private static void inorderTraversal(TreeNode root) {
        if (root == null) {
            return;
        }
        Stack<TreeNode> stack = new Stack<>();
        stack.add(root);

        while (!stack.isEmpty()) {
            TreeNode curTreeNode = stack.pop();
            if (curTreeNode.right != null) {
                stack.add(curTreeNode.right);
            }
            if (curTreeNode.left != null) {
                stack.add(curTreeNode.left);
            }
            System.out.print(curTreeNode.val +" ");
        }
    }

    // 后序
    private static void postorderTraversalRec(TreeNode root) {
        if (root == null) {
            return;
        }
        postorderTraversalRec(root.left);
        postorderTraversalRec(root.right);
        System.out.print(root.val +" ");
    }
}

