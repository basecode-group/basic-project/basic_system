package com.benson.basic.system;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.lang.Dict;
import cn.hutool.json.JSONUtil;
import com.benson.basic.system.entity.Log;

import java.io.File;
import java.text.DecimalFormat;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PathTest {
    public static String PATH = "/Users/zhangbiyu/Documents/Idea_local_workspace/basic-system/target/classes/static/upload";

    public static void main(String[] args) {
//        List<Dict> filePathTree = getFilePathTree("/image");
//        System.out.println(JSONUtil.parseArray(getPath(filePathTree)).toStringPretty());
        long size = FileUtil.size(new File(PATH));
        System.out.println(getNetFileSizeDescription(size));
        System.out.println(size);
    }

    public static String getNetFileSizeDescription(long size) {
        StringBuffer bytes = new StringBuffer();
        DecimalFormat format = new DecimalFormat("###.0");
        if (size >= 1024 * 1024 * 1024) {
            double i = (size / (1024.0 * 1024.0 * 1024.0));
            bytes.append(format.format(i)).append("GB");
        }
        else if (size >= 1024 * 1024) {
            double i = (size / (1024.0 * 1024.0));
            bytes.append(format.format(i)).append("MB");
        }
        else if (size >= 1024) {
            double i = (size / (1024.0));
            bytes.append(format.format(i)).append("KB");
        }
        else if (size < 1024) {
            if (size <= 0) {
                bytes.append("0B");
            }
            else {
                bytes.append((int) size).append("B");
            }
        }
        return bytes.toString();
    }


    public static List<Dict> getPath(List<Dict> parent) {
        if (parent.size() < 1) {
            return parent;
        }
        Dict dict = parent.get(0);
        List<Dict> pathTree = getFilePathTree(dict.getStr("key"));
        if (parent.size() > 0) {
            dict.set("children", getPath(pathTree));
        }
        return parent;
    }

    public static List<Dict> getFilePathTree(String path) {
        String fileSrc = PATH;
        return Stream.of(FileUtil.ls(fileSrc + Optional.ofNullable(path).orElse("")))
                .filter(item -> item.isDirectory())
                .sorted(Comparator.comparing(File::getName).reversed())
                .map(item -> cn.hutool.core.lang.Dict.create()
                        .set("title", item.getName())
                        .set("key", item.getPath().replace(fileSrc, ""))
                )
                .collect(Collectors.toList());
    }
}
